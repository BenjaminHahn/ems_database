/**
 *
 */
package de.hahn.hhn.ipu.mock;

import de.hhn.seb.ipu.app.eventmgmt.Booking;
import de.hhn.seb.ipu.app.eventmgmt.BookingManagement;
import de.hhn.seb.ipu.app.eventmgmt.Event;
import de.hhn.seb.ipu.app.eventmgmt.Session;
import de.hhn.seb.ipu.app.exceptions.InvalidParameterException;
import de.hhn.seb.ipu.app.exceptions.NoReservationPossibleException;
import de.hhn.seb.ipu.app.exceptions.NotAuthorizedException;
import de.hhn.seb.ipu.wnck.mock.infrastructure.userdirectory.User;
import de.hhn.seb.ipu.wnck.mock.infrastructure.userdirectory.UserDirectory;

import java.util.*;
import java.util.logging.Logger;

/**
 * @author Benjamin
 */
public class HahnBookingManagement implements BookingManagement {
    /* Standard class logger. * */
    private static Logger logger = Logger.getLogger(HahnBookingManagement.class
            .getName());
    /* mapping of all existing bookingmgmts for specific event. */
    private static HashMap<Event, HahnBookingManagement> eventsBookings
            = new HashMap<>();
    /* eventId of event bookingmgmt. */
    private final Event representsEvent;
    /* Collection for bookings.*/
    private ArrayList<Booking> bookings = new ArrayList<Booking>();
    /* reference on sessionmgmt. */
    private HahnSessionManagement sessionmgmt;
    /* number of free seats. */
    private int participantsLeft;

    /**
     * maps the bookingmanagement to a specific event needs a event to set the
     * management for.
     *
     * @param event event the bookingmgmt represents
     * @throws InvalidParameterException if something goes wrong with the event
     */
    public HahnBookingManagement(final Event event,
                                 final HahnSessionManagement smgmt) throws InvalidParameterException {
        if (event == null) {
            logger.warning("Something went wrong with this Event");
            throw new InvalidParameterException(); // event should not be null
        }
        if (eventsBookings.containsKey(event)) {
            logger.warning("A BookingManagement already exists for "
                    + event.getId());
            throw new InvalidParameterException(); // throw Exception if
            // bookingmanagement already
            // exists
        } else {
            representsEvent = event; // set event bookingmgmt represents
            participantsLeft = event.getMaxNoParticipants();
            eventsBookings.put(event, this); // add mapping
            sessionmgmt = smgmt;
            logger.fine("BookinggManagement created for " + event.getId());
        }
    }

    /**
     * just for testing clears booking collection.
     */
    public final void removeBookings() {
        bookings.clear();
        logger.info("all Bookings removed from BookingManagement "
                + representsEvent.getId());
    }

    /**
     * just for testing.
     *
     * @return Booking
     */
    public final ArrayList<Booking> getBookingCollection() {
        return bookings;
    }

    /**
     * just for testing.
     *
     * @return representsEvent
     */
    public final Event getRepresentsEvent() {
        return representsEvent;
    }

    @Override
    public final void addBooking(final Booking booking)
            throws NoReservationPossibleException {
        String eventid = booking.getEventId();

        // reservation is not possible
        if (!representsEvent.isReservationPossible()) {
            logger.warning("isReservationPossible: "
                    + representsEvent.isReservationPossible()
                    + " eventId_booking: " + eventid + " eventId_bookingmgmt: "
                    + representsEvent.getId());
            throw new NoReservationPossibleException(); // event forbids booking
        }
        // booking don't matches event
        if (!representsEvent.getId().equals(eventid)) {
            logger.warning("tried to add booking to wrong event");
            throw new NoReservationPossibleException();
        }

        int maxNoEvent = representsEvent.getMaxNoParticipants();
        int actNoEvent = representsEvent.getNoActualParticipants();
        int actNoBooking = booking.getNoParticipants();
        int freeNo = maxNoEvent - actNoEvent;

        // check enough free seats
        if (actNoBooking > freeNo) {
            logger.warning("not enough free participants");
            throw new NoReservationPossibleException();
        } else {
            // Add booking to event
            bookings.add(booking);
            // update participants
            representsEvent.setNoActualParticipants(actNoEvent + actNoBooking);
            // check reservation possible
            updateEvent();
        }
    }

    @Override
    public final Collection<Booking> getBookings(final Session session) {
        Collection<Booking> userBookings = new ArrayList<Booking>(); // list to
        // return
        User user = HahnSessionManagement.getUserForValue(session); // get user
        // via
        // session
        String username = user.getUsername(); // get username via user

        Iterator<Booking> it = bookings.listIterator();
        while (it.hasNext()) {
            Booking booking = (Booking) it.next();
            // test role
            if (sessionmgmt.hasRole(session, UserDirectory.ROLE_EVENTMANAGER)) {
                userBookings.add(booking); // return all bookings
            } else { // else return just the bookings
                if (booking.getUserName().equals(username)) { // of specific
                    // user
                    userBookings.add(booking);
                }
            }

        }
        return userBookings;
    }

    @Override
    public final void removeBooking(final Session session,
                                    final String bookingid) throws InvalidParameterException,
            NotAuthorizedException {

        if (session == null || bookingid == null) {
            throw new InvalidParameterException(); // null reference
        }
        // get username of session
        User user = sessionmgmt.getUserViaSession(session);
        String username = user.getUsername();

        for(Booking booking : bookings) {
            if (booking.getId().equals(bookingid)) {
                // check role
                if (sessionmgmt.hasRole(session,UserDirectory.ROLE_EVENTMANAGER)
                        || booking.getUserName().equals(username)) {
                    // remove booking and update event
                    bookings.remove(booking);
                    updateEvent();
                    logger.finer("user: " + username + " removed booking: "
                            + booking.getId() + " of event: "
                            + representsEvent.getId() + " from user: "
                            + booking.getUserName());
                    break;
                } else {
                    throw new NotAuthorizedException();
                }
            }
        }
    }

    public void updateEvent() {
        int countParticipants = 0;
        // count participants
        for(Booking booking : bookings) {
            countParticipants += booking.getNoParticipants();
        }
        representsEvent.setNoActualParticipants(countParticipants);

        // check reservation possible
        if (representsEvent.getMaxNoParticipants() - representsEvent.getNoActualParticipants() == 0) {
            representsEvent.setReservationPossible(false);
        } else {
            representsEvent.setReservationPossible(true);
        }
    }
}
