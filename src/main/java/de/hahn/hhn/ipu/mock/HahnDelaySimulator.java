/**
 * 
 */
package de.hahn.hhn.ipu.mock;

import de.hhn.seb.ipu.app.exceptions.InvalidParameterException;
import de.hhn.seb.ipu.app.mock.DelaySimulator;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Benjamin
 * 
 */
public class HahnDelaySimulator implements DelaySimulator {
    /** Standard class logger. **/
    private static Logger logger = Logger.getLogger(HahnBookingManagement.class
            .getName());

    /**
     * minimal time of delay.
     */
    private long minimalDelayTime1;
    /**
     * maximal time of delay.
     */
    private long maximalDelayTime1;

    /**
     * @return the minimalDelayTime1
     */
    public final long getMinimalDelayTime1() {
        return minimalDelayTime1;
    }

    /**
     * @return the maximalDelayTime1
     */
    public final long getMaximalDelayTime1() {
        return maximalDelayTime1;
    }

    /**
     * constructor sets delivered delaytimes.
     * 
     * @param minimalDelayTime
     *            minimal delaytime
     * @param maximalDelayTime
     *            maximal delaytime
     */
    public HahnDelaySimulator(final long minimalDelayTime,
            final long maximalDelayTime) {
        this.minimalDelayTime1 = minimalDelayTime;
        this.maximalDelayTime1 = maximalDelayTime;
    }

    /**
     * defaultconstructor delaytime 1000ms.
     */
    public HahnDelaySimulator() {
        final int delay = 1000;
        minimalDelayTime1 = delay;
        maximalDelayTime1 = delay;
    }

    @Override
    public final void setDelayTimes(final long minimalDelayTime2,
            final long maximalDelayTime2) throws InvalidParameterException {
        if ((minimalDelayTime2 <= maximalDelayTime2)
                && (minimalDelayTime2 >= 0)) {
            minimalDelayTime1 = minimalDelayTime2; // set min delaytime
            maximalDelayTime1 = maximalDelayTime2; // set max delaytime

            logger.log(Level.FINEST, "DelayTimes: " + minimalDelayTime2 + " "
                    + maximalDelayTime2);
        } else {
            logger.severe("Couldn't set DelayTimes");
            throw new InvalidParameterException();
        }
    }

    @Override
    public final void simulateDelay() throws InterruptedException {
        Thread.sleep(getNextDelayTime());
        if (Thread.interrupted()) {
            logger.severe("simulate Delay interrupted");
            throw new InterruptedException();
        }
    }

    @Override
    public final long getNextDelayTime() {
        long time = (long) (minimalDelayTime1 + Math.random()
                * (maximalDelayTime1 - minimalDelayTime1 + 1));
        logger.finest("NextDelayTime: " + time);
        return time;
    }
}
