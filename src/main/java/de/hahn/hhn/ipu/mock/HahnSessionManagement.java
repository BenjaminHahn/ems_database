/**
 * 
 */
package de.hahn.hhn.ipu.mock;

import de.hhn.seb.ipu.app.eventmgmt.Session;
import de.hhn.seb.ipu.app.eventmgmt.SessionManagement;
import de.hhn.seb.ipu.app.exceptions.InvalidParameterException;
import de.hhn.seb.ipu.app.exceptions.InvalidSessionException;
import de.hhn.seb.ipu.app.exceptions.NotAuthorizedException;
import de.hhn.seb.ipu.wnck.mock.infrastructure.userdirectory.User;
import de.hhn.seb.ipu.wnck.mock.infrastructure.userdirectory.UserDirectory;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.logging.Logger;

/**
 * @author Benjamin
 * 
 */
public final class HahnSessionManagement implements SessionManagement {
    /** Standard class logger. **/
    private static Logger logger = Logger.getLogger(HahnSessionManagement.class
            .getName());

    /**
     * represents user database.
     */
    private static UserDirectory directory = UserDirectory.getReference();
    /**
     * collection of all active sessions.
     */
    private static HashMap<User, Session> allSessions =
            new HashMap<User, Session>();
    /**
     * singleton reference.
     */
    private static HahnSessionManagement self;

    /**
     * return all active sessions.
     * 
     * @return allSessions all sessions hold by the service
     */
    public HashMap<User, Session> getSessions() {
        return allSessions;
    }

    /**
     * Logs a user into the system. The session is a temporary passport for
     * further interactions. If a user logs in several times without logging
     * out, the user gets always the same session object back.
     * 
     * @param username
     *            name of the user. The user name must be a readable string.
     * @param password
     *            password of the user. The password must be a readable string.
     * @return session of the user
     * @throws InvalidParameterException
     *             if the user name or the password are not valid.
     */
    @Override
    public Session login(final String username, final String password)
            throws InvalidParameterException {
        String directoryPassword;
        User user;

        if (username == null || password == null) {
            throw new InvalidParameterException(); // invalid parameter
        }

        if (username.length() > 0 && // if name and pw not empty
                password.length() > 0) {
            user = directory.getUser(username); // get user with that username
            directoryPassword = user.getPassword(); // get pw of this user
        } else { // else throw Exception
            throw new InvalidParameterException();
        }

        if (password.equals(directoryPassword)) { // if passworts are the same

            if (allSessions.containsKey(user)) { // if user already has a
                                                 // season...
                return allSessions.get(user); // return his season
            } else { // else create,save and return new one
                Session session = new Session(user.getUsername());
                allSessions.put(user, session);
                return session;
            }
        } else { // if pw are not the same throw exception
            throw new InvalidParameterException();
        }
    }

    /**
     * Logs the user out of the system. The session becomes invalid.
     * 
     * @param session
     *            session of the user.
     * @throws InvalidSessionException
     *             if the session is not valid.
     */
    @Override
    public void logout(final Session session) throws InvalidSessionException {
        if (session == null) {
            throw new InvalidSessionException();
        }

        boolean allreadyremoved = allSessions.values().remove(session);
        if (!allreadyremoved) {
            throw new InvalidSessionException(); // if session don't exists
        }
    }

    /**
     * Checks if the given session is a valid session, meaning, that it
     * represents a user which is actually logged in.
     * 
     * @param session
     *            session to be checked
     * @throws InvalidSessionException
     *             if the session is not valid.
     */
    @Override
    public void checkSession(final Session session)
            throws InvalidSessionException {
        if (!allSessions.containsValue(session)) { // if allSessions contains
                                                   // session.. the user is
                                                   // actually logged in
            throw new InvalidSessionException();
        }
    }

    /**
     * Checks if a user, represented by his session, has a given role.
     * 
     * @param session
     *            session of the user
     * @param role
     *            role which has to be owned by the user
     * @throws NotAuthorizedException
     *             if the user does not own this role
     * 
     * @see SessionManagement#hasRole(Session,String)
     */
    @Override
    public void checkAuthorization(final Session session, final String role)
            throws NotAuthorizedException {

        User user = getUserForValue(session); // get user
        Collection<String> roles = user.getRoles(); // get roles
        if (!roles.contains(role)) { // user has not role
            throw new NotAuthorizedException(); // exception
        }
    }

    /**
     * Returns a user object representing the user which belongs to the session.
     * 
     * @param session
     *            session the user object is requested for
     * @return user object of the user belonging to this session
     */
    @Override
    public User getUserViaSession(final Session session) {
        return getUserForValue(session);
    }

    /**
     * Checks if a user, represented by his session, has a given role.
     * 
     * @param session
     *            session of the user
     * @param role
     *            role which has to be owned by the user
     * @return true if the user owns the role, false otherwise
     * 
     * @see SessionManagement#checkAuthorization(Session, String)
     */
    @Override
    public boolean hasRole(final Session session, final String role) {
        User user = getUserForValue(session); // get user
        Collection<String> roles = user.getRoles(); // check if user has role
        if (roles.contains(role)) {
            return true;
        }
        return false;
    }

    /**
     * 
     * @param session
     *            session of the user
     * @return User returns the user of the session
     */
    public static User getUserForValue(final Session session) {

        Iterator<Entry<User, Session>> it = allSessions.entrySet().iterator();
        while (it.hasNext()) {
            Entry<User, Session> pairs = (Entry) it.next();
            User key = (User) pairs.getKey();
            Session value2 = (Session) pairs.getValue();
            if (value2.equals(session)) { // contains value
                return key;
            }
        }
        return null;
    }

    /**
     * get reference on HahnSessionManagement.
     * 
     * @return self
     */
    public static HahnSessionManagement getReference() {
        if (self == null) {
            self = new HahnSessionManagement();
        }
        return self;
    }
}
