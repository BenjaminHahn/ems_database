/**
 *
 */
package de.hahn.hhn.ipu.mock;

import de.hhn.seb.ipu.app.eventmgmt.Booking;
import de.hhn.seb.ipu.app.eventmgmt.Event;
import de.hhn.seb.ipu.app.eventmgmt.EventManagementService;
import de.hhn.seb.ipu.app.eventmgmt.Location;
import de.hhn.seb.ipu.app.eventmgmt.Session;
import de.hhn.seb.ipu.app.exceptions.InvalidParameterException;
import de.hhn.seb.ipu.app.exceptions.InvalidSessionException;
import de.hhn.seb.ipu.app.exceptions.NoReservationPossibleException;
import de.hhn.seb.ipu.app.exceptions.NotAuthorizedException;
import de.hhn.seb.ipu.app.mock.DelaySimulator;
import de.hhn.seb.ipu.app.mock.eventmgmt.EventManagementMockService;
import de.hhn.seb.ipu.wnck.mock.infrastructure.userdirectory.User;
import de.hhn.seb.ipu.wnck.mock.infrastructure.userdirectory.UserDirectory;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Random;
import java.util.logging.Logger;

/**
 * @author Benjamin
 */
public class HahnEventManagement implements EventManagementMockService,
        EventManagementService {
    /* Standard class logger. */
    private static Logger logger = Logger.getLogger(HahnEventManagement.class
            .getName());
    /* collection contains all events. */
    private LinkedList<Event> listEvents = new LinkedList<Event>();
    /* the sessionmgmt managing the logins. */
    private HahnSessionManagement smgmt = new HahnSessionManagement();
    /* Collection containing all bookingmgmts. */
    private HashMap<String, HahnBookingManagement> eventsBookingsMapping = new HashMap<>();
    /* the used delaysimulator. */
    private de.hahn.hhn.ipu.mock.HahnDelaySimulator delaySim = new de.hahn.hhn.ipu.mock.HahnDelaySimulator();
    /* contains all locations */
    private Location[] locations;

    /**
     * default constructor
     */
    public HahnEventManagement() {
        try {
            delaySim.setDelayTimes(0, 0);
            fillInMockData();
            delaySim.setDelayTimes(800, 2500);
        } catch (InvalidParameterException e) {
            e.printStackTrace();
        }
    }

    /**
     * just for testing getAllEvents.
     *
     * @return allEvent
     */
    public final LinkedList<Event> getAllEvents() {
        simulateDelay();
        return listEvents;
    }

    @Override
    public final DelaySimulator getDelaySimulator() {
        return delaySim;
    }

    @Override
    public final void setDelaySimulator(final DelaySimulator simulator)
            throws InvalidParameterException {
        if (simulator != null) {
            delaySim = (de.hahn.hhn.ipu.mock.HahnDelaySimulator) simulator; // substitues actual
            // simulator

        } else {
            throw new InvalidParameterException();
        }
    }

    @Override
    public final void simulateDelay() {
        try {
            delaySim.simulateDelay();
        } catch (InterruptedException e) {
            logger.warning("Delay Simulation was interrupted");
        }
    }

    @Override
    public final Session login(final String username, final String password)
            throws InvalidParameterException {
        logger.info("attempt login username: " + username + " password: " + password);
        simulateDelay();
        return smgmt.login(username, password);
    }

    @Override
    public final void logout(final Session session)
            throws InvalidSessionException {
        simulateDelay();
        smgmt.logout(session);
        logger.info("logout perfomed");
    }

    @Override
    public final void addEvent(final Session session, final Event event)
            throws InvalidParameterException, InvalidSessionException,
            NotAuthorizedException {
        simulateDelay();
        boolean eventExists = false; // true if event exists

        // lock if event exists
        Iterator<Event> it = listEvents.listIterator();
        while (it.hasNext()) {
            Event eventInList = it.next();
            if (eventInList.getId().equals(event.getId())) {
                eventExists = true;
            }
        }

        if (!eventExists) {
            smgmt.checkSession(session);
            if (event == null) {
                logger.warning("event was null");
                throw new InvalidParameterException();
            }
            // add event only when eventmanager
            if (smgmt.hasRole(session, UserDirectory.ROLE_EVENTMANAGER)) {
                listEvents.add(event);
                // add and create bookingmgmt
                HahnBookingManagement bmgmt = new HahnBookingManagement(event,
                        HahnSessionManagement.getReference());
                eventsBookingsMapping.put(event.getId(), bmgmt);
                //listBookingMgmts.add(bmgmt);
                logger.info("added event with title: " + event.getTitle() + " and bmgmt: " + bmgmt);
            } else {
                logger.warning("user is not authorized");
                throw new NotAuthorizedException(); // throw exception otherwise
            }
        } else {
            logger.info("event already existing");
        }
    }

    /**
     * Returns a collection of the events hold by the service.
     *
     * @return Collection of event objects
     */
    @Override
    public final Collection<Event> getEvents() {
        simulateDelay();
        return listEvents;
    }

    @Override
    public final void removeEvent(final Session session, final String eventid)
            throws InvalidParameterException, InvalidSessionException,
            NotAuthorizedException {
        simulateDelay();
        if (smgmt.hasRole(session, UserDirectory.ROLE_EVENTMANAGER)) {
            ListIterator<Event> it = listEvents.listIterator();
            while (it.hasNext()) { // iterate through listEvents
                Event event = it.next();
                // event matches remove it
                if (event.getId().equals(eventid)) {
                    listEvents.remove(event);
                    eventsBookingsMapping.remove(event.getId());
                    //listBookingMgmts.remove(getBookingManagementForEvent(event.getId()));
                    logger.info(event.getTitle() + " removed");
                    break;
                }
            }
        } else {
            throw new NotAuthorizedException();
        }
    }

    @Override
    public final void addBooking(final Session session, final Booking booking)
            throws InvalidParameterException, InvalidSessionException,
            NotAuthorizedException, NoReservationPossibleException {
        simulateDelay();
        // booking is null
        if (booking == null) {
            logger.warning("booking is invalid");
            throw new InvalidParameterException();
        }

        // get bmgmt
        HahnBookingManagement bmgmt = eventsBookingsMapping.get(booking.getEventId());
        logger.info("searched for bmgmt of event: " + getEventViaId(booking.getEventId()).getId() + " bmgmt: " + bmgmt);
        Event event = bmgmt.getRepresentsEvent();

        // event is closed
        if (!event.isReservationPossible()) {
            logger.warning("it's not possible to add a booking to this event");
            throw new NoReservationPossibleException();
        }

        smgmt.checkSession(session); // validate session
        if (!smgmt.hasRole(session, UserDirectory.ROLE_USER)) {
            // if user has not role user throw exception
            logger.warning("user has not the role user");
            throw new NotAuthorizedException();
        }
        // adds user to booking
        User user = smgmt.getUserViaSession(session);
        // via session
        booking.setUserName(user.getUsername());
        bmgmt.addBooking(booking);
        // check booking outbooked
    }

    @Override
    public final void removeBooking(final Session session,
                                    final String eventid, final String bookingid)
            throws InvalidParameterException, InvalidSessionException,
            NotAuthorizedException {
        simulateDelay();
        HahnBookingManagement hbmgmt = eventsBookingsMapping.get(eventid);
        hbmgmt.removeBooking(session, bookingid);
    }

    @Override
    public final Collection<Booking> getBookingsForEvent(final Session session,
                                                         final String eventid) throws InvalidParameterException,
            InvalidSessionException, NoReservationPossibleException {
        Collection<Booking> bookings = null;
        simulateDelay();
        smgmt.checkSession(session); // check validation of session
        // get bookingManagement of event
        HahnBookingManagement bmgmt = eventsBookingsMapping.get(eventid);
        Event event = (bmgmt != null) ? bmgmt.getRepresentsEvent() : null;

        if (event == null) {
            logger.warning("there is no such event");
            throw new InvalidParameterException(); // if theres no such event
        }

        bookings = bmgmt.getBookings(session); // get bookings
        return bookings;
    }

    @Override
    public User getUser(Session session) throws InvalidSessionException {
        return smgmt.getUserViaSession(session);
    }

    /**
     * return user object for session
     *
     * @param session the actuall session of the user
     * @return user object containing data
     */
    public User getUserViaSession(Session session) {
        simulateDelay();
        return smgmt.getUserViaSession(session);
    }

    /**
     * get a event via the event id
     *
     * @param id of the event
     * @return event object
     */
    private Event getEventViaId(String id) {
        for (Event event : getAllEvents()) {
            if (event.getId().equals(id)) {
                return event;
            }
        }
        return null;
    }

    @Override
    /*
    * @param username2  username of the user
    * @param password2  password of the user
    * @param firstname2 firstname of the user
    * @param lastname2  lastname of the user
    * @param roles2     String array naming all roles. May be empty or null*/
    public final void fillInMockData() {
        Random random = new Random();
        UserDirectory directory = UserDirectory.getReference();
        directory.addUser(new User("charles", "secret", "charles", "xavier", UserDirectory.ROLES_SIMPLE));
        directory.addUser(new User("logan", "secret", "logan", "howlett", UserDirectory.ROLES_SIMPLE));
        directory.addUser(new User("ororo", "secret", "ororo", "Iqadi t’challa", UserDirectory.ROLES_SIMPLE));
        String[] usernames = new String[]{"peter", "paul", "mary", "charles", "logan", "ororo"};
        try {
            locations = new Location[]{
                    new Location("Württembergische Landesbibliothek", "Mit über 5,7 Millionen Medien – darunter viele kulturelle " +
                            "Schätze und bedeutende Sammlungen – ist " +
                            "die Württembergische Landesbibliothek ein zentraler " +
                            "Informationsdienstleis ter der Region Stuttgart und " +
                            "ein bedeutendes Zentrum der Wissenschaft, Forschung " +
                            "und Bildung in Baden-Württemberg. ", new URL("http://www.wlb-stuttgart.de/")),
                    new Location("Hauptstaatsarchiv", "Eine Schatzkammer der württembergischen Geschichte: " +
                            "Das Hauptstaatsarchiv verwahrt in seinen " +
                            "Magazinen über 24 Kilometer einmaliges und wertvolles " +
                            "Archivgut, darunter über 100.000 Urkunden, " +
                            "Staatsverträge, Briefe, Fotos und Karten. ", new URL("http://www.landesarchiv-bw.de/web/")),
                    new Location("Staatsgalerie Stuttgart", "Die spektakuläre Schau bringt die wegweisenden, " +
                            "späten Meisterwerke von drei ganz großen Malern " +
                            "des 19. bis 21. Jahrhunderts erstmals zusammen. " +
                            "William Turner, Claude Monet und Cy Twombly haben " +
                            "in ihrer Zeit jeweils die Grenzen der Malerei ausgelotet " +
                            "und radikal neue Wege beschritten. ", new URL("http://www.staatsgalerie.de/"))
            };
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        Date date;
        Event[] events =
                new Event[]{
                        new Event("Führung durch das Magazin", "Führungen gewähren Besuchern einzigartige Einblicke " +
                                "in die Arbeit und die Schätze des Hauses. ", locations[(randomInt(0, locations.length - 1))], date = randomDate(), randomHigherDate(date)),
                        new Event("Live-Rock-Pop mit Eric Gauthier & Band", "Der Franko-Kanadier Eric Gauthier ist ein international " +
                                "gefragter Choreograph, Leiter von „Gauthier Dance“, " +
                                "dem furiosen Tanzensemble des Stuttgarter Theaterhauses " +
                                "und auch mit seiner 2001 gegründeten Band " +
                                "eine feste kulturelle Größe in der Region. Die musikalische " +
                                "Mischung aus Britpop, Rock, energiegeladenen " +
                                "Songs, romantischen Balladen und Gauthier s" +
                                "witzigen sowie poetischen Texten begeistert eine " +
                                "ständig wachsende Fangemeinde. ", locations[(randomInt(0, locations.length - 1))],
                                date = randomDate(), randomHigherDate(date)),
                        new Event("Führung durch die Digitalisierungs-Werkstatt", "Willkommen in der Welt der Digitalisierung, vom Grazer " +
                                "Buchtisch bis zum Scan-Roboter. (30 Min.) ", locations[(randomInt(0, locations.length - 1))],
                                date = randomDate(), randomHigherDate(date)),
                        new Event("U21-Führungen", "Teens treffen sich im Foyer der Neuen Staatsgalerie " +
                                "zu den beliebten U 21-Führungen mit Aktionen rund " +
                                "um die Sammlung und die Sonderausstellung. ", locations[(randomInt(0, locations.length - 1))],
                                date = randomDate(), randomHigherDate(date)),
                        new Event("Ausstellungsführungen", "Halbstündige Führungen durch die Sonderausstellung " +
                                "„Turner, Monet, Twombly – Later Paintings“ " +
                                "und Stippvisiten durch die ständige Sammlung. ", locations[(randomInt(0, locations.length - 1))],
                                date = randomDate(), randomHigherDate(date)),
                        new Event("Werkbesprechung \"Walter De Maria\"", "Der „25m Stab“ von Walter de Maria im Wechselausstellungsraum " +
                                "ist eine golden schimmernde Skulptur " +
                                "aus 50 perfekt gearbeiteten und miteinander verbundenen " +
                                "Zylindern. Sie vereint Konzentration und " +
                                "Lösung, Leichtigkeit und Schwere, Statik und Dynamik, " +
                                "Abgeschlossenheit und Verspannung. Die elegante " +
                                "Form lässt sein tatsächliches Gewicht von fünf " +
                                "Tonnen kaum erahnen. Besucher erhalten eine „Sehhilfe“ " +
                                "für das Hauptwerk des Grenzgängers zwischen " +
                                "den drei Genres Land, Minimal und Concept Art. " +
                                "(Stirling Halle)", locations[(randomInt(0, locations.length - 1))],
                                date = randomDate(), randomHigherDate(date))
                };
        Session session = null;
        try {
            int[] numbers = new int[]{5, 10, 15, 20, 25, 30};
            // add all events
            session = smgmt.login("peter", "secret");
            for (Event event : events) {
                event.setReservationPossible(true);
                event.setMaxNoParticipants(numbers[randomInt(0, numbers.length - 1)]);
                addEvent(session, event);
            }
            // try to add a booking to every event
            for (int i = 0; i < 15; i++) {
                session = smgmt.login(usernames[randomInt(1, usernames.length - 1)], "secret");
                Event event = events[(randomInt(0, events.length - 1))];
                int participants = random.nextInt(6) + 1;
                if (event.getMaxNoParticipants() - event.getNoActualParticipants() > participants) {
                    Booking booking = new Booking(event.getId(), participants);
                    addBooking(session, booking);
                }
            }
            // set 2 random events on allDay and on indefinite participants
            for (int i = 0; i < 2; i++) {
                events[randomInt(0, events.length - 1)].setAllDayEvent(true);
                events[randomInt(0, events.length - 1)].setMaxNoParticipants(Integer.MAX_VALUE);
            }

        } catch (InvalidParameterException e) {
            e.printStackTrace();
        } catch (NotAuthorizedException e) {
            e.printStackTrace();
        } catch (InvalidSessionException e) {
            e.printStackTrace();
        } catch (NoReservationPossibleException e) {
            e.printStackTrace();
        }
    }

    /**
     * get locations hold by the service
     *
     * @return list of all locations
     */
    public List getLocations() {
        simulateDelay();
        return Arrays.asList(locations);
    }

    private int randomInt(int min, int max) {
        final int number = (int) (min + (Math.random() * (max - min + 1)));
        return number;
    }

    private Date randomDate() {
        Date date = new Date(System.currentTimeMillis());
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        int year = randomInt(calendar.get(Calendar.YEAR), calendar.get(Calendar.YEAR) + 5);
        int month = randomInt(0, 12);
        int day = randomInt(0, 28);
        int hour = randomInt(16, 20);
        int minute = 0;

        calendar.set(year, month, day, hour, minute);
        return calendar.getTime();
    }

    private Date randomHigherDate(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DAY_OF_MONTH) + randomInt(0, 1);
        int hour = calendar.get(Calendar.DAY_OF_MONTH) == day ? calendar.get(Calendar.HOUR_OF_DAY) + randomInt(1, 3) : randomInt(0, 3);
        int minute = calendar.get(Calendar.MINUTE);

        calendar.set(year, month, day, hour, minute);
        return calendar.getTime();
    }
}
