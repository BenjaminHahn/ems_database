package de.hahn.hhn.ipu.jmx;

import java.lang.management.ManagementFactory;
import java.util.logging.Logger;

import javax.management.InstanceAlreadyExistsException;
import javax.management.MBeanRegistrationException;
import javax.management.MBeanServer;
import javax.management.MalformedObjectNameException;
import javax.management.NotCompliantMBeanException;
import javax.management.ObjectName;

import com.sun.jdmk.comm.HtmlAdaptorServer;

/**
 * 
 * class provides methods to create and stop an Http-Adapter.
 * 
 * @author Ben
 * 
 */
final class SimpleJXMAgent {
    /**
     * default logger.
     */
    private static final Logger logger = Logger.getLogger(SimpleJXMAgent.class
            .getName());

    /**
     * singleton reference.
     */
    private static SimpleJXMAgent self;
    /**
     * the MBean-Server hosting the page.
     */
    private MBeanServer mbs = null;
    /**
     * the adapter witch sets the log levels.
     */
    private static HtmlAdaptorServer adapter;

    /**
     * constructor creates and registers MBean-Server.
     * 
     * @throws Exception
     */
    private SimpleJXMAgent() {

        mbs = ManagementFactory.getPlatformMBeanServer();
        ObjectName name;
        try {
            name = new ObjectName(
                    "de.hahn.hhn.ipu.logging:type=LoggingHelperJMXAdapter");
            LoggingHelperJMXAdapter mbean = new LoggingHelperJMXAdapter();
            mbs.registerMBean(mbean, name);
            final int port = 7000;

            createHTTPAdaptor(port);
        } catch (MalformedObjectNameException e) {
            logger.warning("The format of the string does not"
                    + " correspond to a valid ObjectName.");
        } catch (InstanceAlreadyExistsException e) {
            logger.warning("The MBean is already "
                    + "registered in the repository.");
        } catch (MBeanRegistrationException e) {
            logger.warning("Wraps exceptions thrown by the preRegister(),"
                    + " preDeregister() methods "
                    + "of the MBeanRegistration interface.");
        } catch (NotCompliantMBeanException e) {
            logger.warning("Exception which occurs when trying "
                    + "to register an object inthe MBean server"
                    + " that is not a JMX compliant MBean.");
        }

    }

    /**
     * creates a new adapter and registers the MBean-Server.
     * 
     * @param port
     *            default port used
     * @throws InstanceAlreadyExistsException
     *             The MBean is already registered in the repository.
     * @throws MBeanRegistrationException
     *             Wraps exceptions thrown by the preRegister(), preDeregister()
     *             methods of the MBeanRegistration interface.
     * @throws NotCompliantMBeanException
     *             Exception which occurs when trying to register an object in
     *             the MBean server that is not a JMX compliant MBean.
     * @throws MalformedObjectNameException
     *             The format of the string does not correspond to a valid
     *             ObjectName.
     */
    public void createHTTPAdaptor(final int port)
            throws InstanceAlreadyExistsException, MBeanRegistrationException,
            NotCompliantMBeanException, MalformedObjectNameException {

        adapter = new HtmlAdaptorServer();
        adapter.setPort(port);
        ObjectName adapterName;
        adapterName = new ObjectName("Adaptor" + ":name=htmladapter,port="
                + port);
        mbs.registerMBean(adapter, adapterName);
        adapter.start();
    }

    /**
     * stops the adapter.
     */
    public static void stopAdapter() {
        adapter.stop();
    }

    /**
     * reference on this singleton.
     * 
     * @return self
     */
    public static SimpleJXMAgent getReference() {
        if (self == null) {
            self = new SimpleJXMAgent();
            return self;
        } else {
            return self;
        }

    }
}
