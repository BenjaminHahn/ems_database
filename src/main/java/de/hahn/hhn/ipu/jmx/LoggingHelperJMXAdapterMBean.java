package de.hahn.hhn.ipu.jmx;

/**
 * Interface for JMX accessible methods of the LoggingHelperJMXAdapter.
 * 
 * @author wnck
 * 
 */
public interface LoggingHelperJMXAdapterMBean {

    /**
     * set finest log level.
     */
    void setFinest();

    /**
     * set info log level.
     */
    void setInfo();

    /**
     * set warning log level.
     */
    void setWarning();

    /**
     * set severe log level.
     */
    void setSevere();

    /**
     * set finer log level.
     */
    void setFiner();

    /**
     * set fine log level.
     */
    void setFine();

    /**
     * set consolelinemode log level.
     */
    void toggleConsoleLines();

}
