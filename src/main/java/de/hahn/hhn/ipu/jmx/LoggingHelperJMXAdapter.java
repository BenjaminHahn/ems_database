package de.hahn.hhn.ipu.jmx;

import de.hahn.hhn.ipu.logging.HahnLoggingHelper;
import de.hhn.seb.ipu.app.exceptions.InvalidParameterException;
import de.hhn.seb.pp.logging.LoggingHelperNotInitializedException;

import java.util.logging.Level;

/**
 * Adapterclass to set LoggingHelper.
 * 
 * @author hahn
 * 
 */
public class LoggingHelperJMXAdapter implements LoggingHelperJMXAdapterMBean {

    /**
     * reference on LoggingHelper implementation.
     */
    private HahnLoggingHelper helper = HahnLoggingHelper.getReference();

    @Override
    public final void setFinest() {
        try {
            helper.setLogLevel(Level.FINEST);
        } catch (LoggingHelperNotInitializedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (InvalidParameterException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    @Override
    public final void setInfo() {
        try {
            helper.setLogLevel(Level.INFO);
        } catch (LoggingHelperNotInitializedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (InvalidParameterException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    @Override
    public final void setWarning() {
        try {
            helper.setLogLevel(Level.WARNING);
        } catch (LoggingHelperNotInitializedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (InvalidParameterException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    @Override
    public final void setSevere() {
        try {
            helper.setLogLevel(Level.SEVERE);
        } catch (LoggingHelperNotInitializedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (InvalidParameterException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    @Override
    public final void setFiner() {
        try {
            helper.setLogLevel(Level.FINER);
        } catch (LoggingHelperNotInitializedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (InvalidParameterException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    @Override
    public final void setFine() {
        try {
            helper.setLogLevel(Level.FINE);
        } catch (LoggingHelperNotInitializedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (InvalidParameterException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    @Override
    public final void toggleConsoleLines() {
        if (helper.isConsoleInSingleLineMode()) {
            helper.setConsoleInSingleLineMode(false);

        } else {
            helper.setConsoleInSingleLineMode(true);

        }
    }

}
