/**
 * 
 */
package de.hahn.hhn.ipu.logging;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.logging.ConsoleHandler;
import java.util.logging.FileHandler;
import java.util.logging.Filter;
import java.util.logging.Formatter;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.LogManager;
import java.util.logging.LogRecord;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

import de.hhn.seb.ipu.app.exceptions.InvalidParameterException;
import de.hhn.seb.pp.logging.FileHandlerAlreadyInstantiatedException;
import de.hhn.seb.pp.logging.LoggingHelper;
import de.hhn.seb.pp.logging.LoggingHelperAlreadyInitializedException;
import de.hhn.seb.pp.logging.LoggingHelperNotInitializedException;
import de.hhn.seb.pp.logging.WhiteListBlackListConflictException;

/**
 * implementation of the LoggingHelper.
 * 
 * @author Ben
 * 
 */
public final class HahnLoggingHelper implements LoggingHelper {
    /** Standard class logger. **/
    private static Logger logger = Logger.getLogger(HahnLoggingHelper.class
            .getName());

    /**
     * reference on only object.
     */
    private static HahnLoggingHelper self = null;
    /**
     * reference on root logger.
     */
    private Logger rootLogger = Logger.getLogger("");
    /**
     * true if logger initialized.
     */
    private boolean loggerInitialized = false;
    /**
     * true if filehandler was created.
     */
    private boolean fileHandlerInitiated = false;
    /**
     * represents the actual loglevel of the service.
     */
    private Level actualLogLevel = Level.INFO;
    /**
     * true if service is in single line mode.
     */
    private boolean singleLineMode = false;
    /**
     * true if whitelist is in use.
     */
    private boolean whiteListSet = false;
    /**
     * true if blacklist is in use.
     */
    private boolean blackListSet = false;
    /**
     * array contains all allowed logger prefixes.
     */
    private ArrayList<String> whiteListArray = new ArrayList<String>();
    /**
     * array contains all non allowed logger prefixes.
     */
    private ArrayList<String> BlackListArray = new ArrayList<String>();
    /**
     * saves the default formatter.
     */
    private Formatter defaultFormatter;
    /**
     * name for filehandler.
     */
    private String appName;
    /**
     * filter for setting white and blacklist approach.
     */
    private WhitelistBlacklistFilter myFilter = new WhitelistBlacklistFilter();
    /**
     * filter that all logger can log.
     */
    private AllTrueFilter trueFilter = new AllTrueFilter();
    /**
     * flter that no logger can log.
     */
    private AllFalseFilter falseFilter = new AllFalseFilter();

    /**
     * get reference for LoggingHelper.
     * 
     * @return HahnLoggingHelper
     */
    public static HahnLoggingHelper getReference() {
        if (self == null) {
            self = new HahnLoggingHelper(); // return new Logger
        }
        return self; // return actual logger
    }

    /**
     * just for testing.
     * 
     * @return the blackListArray
     */
    public ArrayList<String> getBlackListArray() {
        return BlackListArray;
    }

    /**
     * private constructor.
     */
    private HahnLoggingHelper() {

    }

    @Override
    public LoggingHelper addLoggerPrefixBlackList(final String prefix)
            throws LoggingHelperNotInitializedException,
            InvalidParameterException, WhiteListBlackListConflictException {
        if (!loggerInitialized) {
            throw new LoggingHelperNotInitializedException();
        }

        if (prefix == null) {
            throw new InvalidParameterException();
        }
        if (whiteListSet) {
            throw new WhiteListBlackListConflictException(); // already existing
                                                             // whitelist
        }
        blackListSet = true; // set blacklist
        if (prefix.equals("")) {
            Handler[] handlers = rootLogger.getHandlers();
            for (Handler handler : handlers) {
                handler.setFilter(falseFilter);
            }
        }
        BlackListArray.add(prefix);
        return this;
    }

    @Override
    public LoggingHelper addLoggerPrefixWhiteList(final String prefix)
            throws LoggingHelperNotInitializedException,
            WhiteListBlackListConflictException, InvalidParameterException {
        if (!loggerInitialized) {
            throw new LoggingHelperNotInitializedException();
        }

        if (prefix == null) {
            throw new InvalidParameterException();
        }

        if (blackListSet) {
            throw new WhiteListBlackListConflictException(); // already exiting
                                                             // blacklist
        }
        whiteListSet = true;
        if (prefix.equals("")) {
            Handler[] handlers = rootLogger.getHandlers();
            for (Handler handler : handlers) {
                handler.setFilter(trueFilter);
            }
        }
        whiteListArray.add(prefix);
        return this;
    }

    @Override
    public LoggingHelper createFileHandler()
            throws LoggingHelperNotInitializedException, IOException,
            FileHandlerAlreadyInstantiatedException {

        if (!loggerInitialized) {
            throw new LoggingHelperNotInitializedException(); // no
                                                              // LoggingHelper
                                                              // initiated
        }
        if (fileHandlerInitiated) {
            throw new FileHandlerAlreadyInstantiatedException(); // FileHandler
                                                                 // already
                                                                 // existing
        }

        File file = new File("logs");
        if (!file.exists()) {
            if (!file.mkdir()) {
                logger.warning("Could not create directory");
            }
        }

        Handler fileHandler = new FileHandler(file.getAbsolutePath() + "\\"
                + appName + ".log"); // create

        fileHandler.setFilter(myFilter);
        rootLogger.addHandler(fileHandler); // add handler to logger
        fileHandlerInitiated = true;

        return this;
    }

    @Override
    public Level getLogLevel() throws LoggingHelperNotInitializedException {

        if (!loggerInitialized) {
            throw new LoggingHelperNotInitializedException(); // not initiated
        }
        return actualLogLevel;
    }

    @Override
    public LoggingHelper initialize(final String appName2)
            throws LoggingHelperAlreadyInitializedException,
            InvalidParameterException {

        if (loggerInitialized) {
            throw new LoggingHelperAlreadyInitializedException(); // already
                                                                  // initialized
        }
        if (appName2 == null) {
            throw new InvalidParameterException(); // invalid parameter
        }

        Handler[] handlers = rootLogger.getHandlers();
        for (Handler handler : handlers) {
            handler.setFilter(myFilter);
            handler.setLevel(Level.FINEST);
        }

        rootLogger.setLevel(actualLogLevel);

        this.appName = appName;
        loggerInitialized = true; // logger initialized
        return this;
    }

    @Override
    public boolean isConsoleInSingleLineMode() {
        return singleLineMode;
    }

    @Override
    public LoggingHelper setConsoleInSingleLineMode(final boolean mode) {
        Formatter formatter;

        if (mode) {
            formatter = new SingleLineFormatter(); // set single line
            singleLineMode = true;
        } else {
            formatter = new SimpleFormatter(); // set standard formatter
            singleLineMode = false;
        }
        Handler[] handlers = rootLogger.getHandlers(); // set formatter for
                                                       // every handler
        for (Handler handler : handlers) {
            handler.setFormatter(formatter);
        }
        return this;
    }

    @Override
    public LoggingHelper setLogLevel(final Level level)
            throws LoggingHelperNotInitializedException,
            InvalidParameterException {

        if (!loggerInitialized) {
            throw new LoggingHelperNotInitializedException(); // not initialized
        }
        if (level == null) {
            throw new InvalidParameterException(); // invalid Parameter
        }

        rootLogger.setLevel(level); // set logger Level
        actualLogLevel = level;
        return this;
    }

    /**
     * resets hole logging system.
     */
    public void tearDown() {
        LogManager.getLogManager().reset();
        Handler[] handlers = rootLogger.getHandlers();
        for (Handler handler : handlers) {
            handler.close();
            rootLogger.removeHandler(handler);
        }
        rootLogger.addHandler(new ConsoleHandler());
        for (Handler handler : handlers) {
            handler.setFilter(myFilter);
        }

        self = new HahnLoggingHelper();
    }

    /**
     * filter controls which messages should pass.
     * 
     * @author Ben
     * 
     */
    class WhitelistBlacklistFilter implements Filter {
        @Override
        public boolean isLoggable(final LogRecord record) {

            if (whiteListSet) { // when whiteList set Iterator<String> it =
                Iterator<String> it = whiteListArray.listIterator();
                while (it.hasNext()) {
                    String s = it.next();
                    if (record.getLoggerName().startsWith(s)) {
                        return true;
                    }
                }
                return false;
            }
            if (blackListSet) { // if blacklist set
                Iterator<String> it = BlackListArray.listIterator();
                while (it.hasNext()) {
                    String s = it.next();
                    if (record.getLoggerName().startsWith(s)) {
                        return false;
                    }
                }
                return true;
            }
            return true;
        }
    }

    /**
     * finlter allows all messages to be logged.
     * 
     * @author Ben
     * 
     */
    private static class AllTrueFilter implements Filter {
        @Override
        public boolean isLoggable(final LogRecord arg0) {
            return true;
        }
    }

    /**
     * finlter allows no messages to be logged.
     * 
     * @author Ben
     * 
     */
    private static class AllFalseFilter implements Filter {
        @Override
        public boolean isLoggable(final LogRecord arg0) {
            return false;
        }
    }

    /**
     * single line formatter formats message on a single line includes
     * date,source,level,message,exception.
     */
    private static class SingleLineFormatter extends Formatter {

        @Override
        public String format(final LogRecord record) {
            String source = "";
            SimpleDateFormat simple = new SimpleDateFormat("HH:mm:ss-SSS");

            // concat classname and methode name if not null
            // if both null, concat loggername
            if (record.getSourceClassName() != null) {
                source = record.getLoggerName() + "-->";
                if (record.getSourceMethodName() != null) {
                    source += " " + record.getSourceMethodName();
                }
            }

            String s = simple.format(new Date(record.getMillis())) + " | "
                    + record.getLevel() + " | " + record.getMessage() + " | "
                    + source + "\n";

            return s;
        }
    }
}
