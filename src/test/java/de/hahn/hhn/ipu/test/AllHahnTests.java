package de.hahn.hhn.ipu.test;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({
        TestHahnDelaySimulator.class, 
        TestHahnEventManagement.class, 
        TestHahnBookingManagement.class, 
        TestHahnSessionManagement.class
        })

public class AllHahnTests {

}
