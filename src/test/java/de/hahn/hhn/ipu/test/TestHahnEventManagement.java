package de.hahn.hhn.ipu.test;

import de.hahn.hhn.ipu.logging.HahnLoggingHelper;
import de.hahn.hhn.ipu.mock.HahnDelaySimulator;
import de.hahn.hhn.ipu.mock.HahnEventManagement;
import de.hahn.hhn.ipu.mock.HahnSessionManagement;
import de.hhn.seb.ipu.app.eventmgmt.Booking;
import de.hhn.seb.ipu.app.eventmgmt.Event;
import de.hhn.seb.ipu.app.eventmgmt.Location;
import de.hhn.seb.ipu.app.eventmgmt.Session;
import de.hhn.seb.ipu.app.exceptions.InvalidParameterException;
import de.hhn.seb.ipu.app.exceptions.InvalidSessionException;
import de.hhn.seb.ipu.app.exceptions.NoReservationPossibleException;
import de.hhn.seb.ipu.app.exceptions.NotAuthorizedException;
import de.hhn.seb.pp.logging.LoggingHelperAlreadyInitializedException;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.Collection;
import java.util.Date;
import java.util.logging.Logger;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

public class TestHahnEventManagement {
	/** Standard class logger. **/
	private static Logger logger = Logger
			.getLogger(TestHahnEventManagement.class.getName());

	private HahnEventManagement emgmt;
	private HahnSessionManagement smgmt;
	private Event event1;
	private Event event2;
	
	@BeforeClass
	public static void initializeClass() {
	    HahnLoggingHelper helper = HahnLoggingHelper.getReference();
	    try {
            helper.initialize("blub");
            helper.setConsoleInSingleLineMode(true);
        } catch (LoggingHelperAlreadyInitializedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (InvalidParameterException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
	}
	
	@Before
	public void initialize(){
        emgmt = new HahnEventManagement();
        smgmt = new HahnSessionManagement();
		event1 = new Event("title","description", new Location("name1","description1",null), new Date(), new Date());
		event2 = new Event("title","description", new Location("name2","description2",null), new Date(), new Date());
	}
	
	/**
	 * tests if simulator is replaced
	 */
	@Test
	public void testSetGetDelaySimulator() {
		HahnDelaySimulator sim = new HahnDelaySimulator(); // new simulator
		HahnDelaySimulator actualDelaySim = (HahnDelaySimulator) emgmt.getDelaySimulator();

          /*  TODO assertNotEquals
		assertNotEquals(sim,actualDelaySim);
		try {
			emgmt.setDelaySimulator(sim);
		} catch (InvalidParameterException e) {
			fail();
		}
		actualDelaySim = (HahnDelaySimulator) emgmt.getDelaySimulator();
		assertEquals(sim,actualDelaySim);

		try {
			emgmt.setDelaySimulator(null);	// test null reference
			fail();
		} catch (InvalidParameterException e) {
			// success
		}               */
	}
	
	 @Test
	 public void testAddEventRole() {
				 
		event1 = new Event("title","description", new Location("name1","description1",null), new Date(), new Date());

			
		 try {
			 Session session = smgmt.login("paul","secret");	// test addEvent as user
			 emgmt.addEvent(session,event1);
			 fail();
		 } catch (InvalidParameterException e) {
				fail();
		 } catch (InvalidSessionException e) {
				fail();
		 } catch (NotAuthorizedException e) {			
				// success
		 }
		 assertTrue(emgmt.getEvents().size() == 0);
		 
		 try {
			 Session session = smgmt.login("peter","secret");	// test addEvent as manager
			 emgmt.addEvent(session,event1);
		 } catch (InvalidParameterException e) {
				fail();
		 } catch (InvalidSessionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NotAuthorizedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		 assertTrue(emgmt.getEvents().size() == 1);
	 }
	 
	 
	 
	 
	 @Test
	 public void testGetEventsRemoveEvents(){
		 Session session1 = null;
		 Session session2 = null;

		 try {
			session1 = smgmt.login("peter","secret");	// add Events
			emgmt.addEvent(session1, event1);
			emgmt.addEvent(session1, event2);
		} catch (InvalidParameterException e) {
			fail();
		} catch (InvalidSessionException e) {
			fail();
		} catch (NotAuthorizedException e) {
			fail();
		}
		 assertTrue(emgmt.getEvents().size() == 2);
		 
		 try {
			session2 = smgmt.login("paul","secret");	// try to remove events as user
			emgmt.removeEvent(session2, event1.getId());
			fail();
		 } catch (InvalidParameterException e) {
			fail();
		} catch (InvalidSessionException e) {
			fail();
		} catch (NotAuthorizedException e) {
			// success
		}
		 
		 try {
			emgmt.removeEvent(session1, event1.getId());
		} catch (InvalidParameterException e) {
			fail();
		} catch (InvalidSessionException e) {
			fail();
		} catch (NotAuthorizedException e) {
			fail();
		}
	 	assertTrue(emgmt.getEvents().size() == 1);
		 
	 }
	 
	 @Test
	 public void testAddBookingsGetBookings(){
		 event1.setReservationPossible(true);
		 Booking booking1 = new Booking(event1.getId(),5);
		 booking1.setUserName("paul");
		 Booking booking2 = new Booking(event1.getId(),3);
		 booking2.setUserName("mary");
		 Booking booking3 = new Booking(event1.getId(),2);
		 booking3.setUserName("paul");
		 Session session1 = null;
		 Session session2 = null;
		 Session session3 = null;
		 Collection<Booking> bookingsPeter = null;
		 Collection<Booking> bookingsAll = null;
		 Collection<Booking> bookingsMary = null;
		 
		 try {
			session1 = emgmt.login("peter", "secret");
			emgmt.addEvent(session1, event1);
		} catch (InvalidParameterException | InvalidSessionException | NotAuthorizedException e1) {
			fail();
		}
		 
		 try {
			session2 = emgmt.login("paul", "secret");	// user adds booking
			emgmt.addBooking(session2, booking1);
			emgmt.addBooking(session2, booking3);
			session3 = emgmt.login("mary", "secret");	// user2 adds booking
			emgmt.addBooking(session3, booking2);
		} catch (InvalidParameterException e) {
			fail();
		} catch (InvalidSessionException e) {
			fail();
		} catch (NotAuthorizedException e) {
			fail();
		} catch (NoReservationPossibleException e) {
			fail();
		}
//		 HahnBookingManagement hbmgmt = HahnBookingManagement.getBookingManagementViaEventId(event1.getId());
//		 assertTrue(hbmgmt.getBookingCollection().size() == 3);
		 
		 try {
			 bookingsPeter = emgmt.getBookingsForEvent(session2, event1.getId());	// gets collection of booking back
			 bookingsMary = emgmt.getBookingsForEvent(session3, event1.getId());
			 bookingsAll = emgmt.getBookingsForEvent(session1, event1.getId());
		 } catch (InvalidParameterException e) {
			fail();
		} catch (InvalidSessionException e) {
			fail();
		} catch (NoReservationPossibleException e) {
			fail();
		}
		 
		 event1.setReservationPossible(false);
		 Booking booking4 = new Booking(event1.getId(),1);
		 booking4.setUserName("paul");
		 try {
				session2 = emgmt.login("paul", "secret");	// user adds booking
				emgmt.addBooking(session2, booking4);		// should be impossible
				fail();
			} catch (InvalidParameterException e) {
				fail();
			} catch (InvalidSessionException e) {
				fail();
			} catch (NotAuthorizedException e) {
				fail();
			} catch (NoReservationPossibleException e) {
				//success
			}
		 
		 
		 assertTrue(bookingsPeter.size() == 2);	// paul should get his bookings
		 assertTrue(bookingsMary.size() == 1);
		 assertTrue(bookingsAll.size() == 3);	// peter should get all bookings
	 }
	 
//	 @Test
// 	public void testRemoveBooking(){
//		 Session session1 = null;
//		 Session session2 = null;
//		 Booking booking1 = new Booking(event1.getId(),5);
//		 booking1.setUserName("paul");
//		 event1.setReservationPossible(true);
//
//
//		 try {
//			session1 = smgmt.login("peter", "secret");
//			emgmt.addEvent(session1, event1);
//			session2 = smgmt.login("paul","secret");
//			emgmt.addBooking(session2, booking1);
//		 } catch (InvalidParameterException | InvalidSessionException | NotAuthorizedException | NoReservationPossibleException e) {
//			 fail();
//		 }
//		 HahnBookingManagement hbmgmt = HahnBookingManagement.getBookingManagementViaEventId(event1.getId());
//		 assertTrue(hbmgmt.getBookingCollection().size() == 1);
//
//		 try {
//			emgmt.removeBooking(session2, event1.getId(), booking1.getId());
//		} catch (InvalidParameterException e) {
//			fail();
//		} catch (InvalidSessionException e) {
//			fail();
//		} catch (NotAuthorizedException e) {
//			fail();
//		}
//		 assertTrue(hbmgmt.getBookingCollection().size() == 0);
//	 }
	 
//	 @Ignore
//	 public void mockUpBooking(){
//		 emgmt.fillInMockData();
//		 LinkedList<Event> events = emgmt.getAllEvents();
//		 assertTrue(events.size() == 3);
//		 HahnBookingManagement bmgmt = HahnBookingManagement.getBookingManagementViaEventId(events.getFirst().getId());
//		 assertTrue(bmgmt.getBookingCollection().size() == 2);
//	 }

    @Test
    public void testRandomInt(){

    }
}
