package de.hahn.hhn.ipu.test;

import de.hahn.hhn.ipu.mock.HahnSessionManagement;
import de.hhn.seb.ipu.app.eventmgmt.Session;
import de.hhn.seb.ipu.app.exceptions.InvalidParameterException;
import de.hhn.seb.ipu.app.exceptions.InvalidSessionException;
import de.hhn.seb.ipu.app.exceptions.NotAuthorizedException;
import de.hhn.seb.ipu.wnck.mock.infrastructure.userdirectory.User;
import de.hhn.seb.ipu.wnck.mock.infrastructure.userdirectory.UserDirectory;
import java.util.logging.Logger;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotSame;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import org.junit.Before;
import org.junit.Test;

public class TestHahnSessionManagement {
	/** Standard class logger. **/
	private static Logger logger = Logger
			.getLogger(TestHahnSessionManagement.class.getName());

	private HahnSessionManagement smgmt;
	
	@Before 
	public void initialize(){
		smgmt = new HahnSessionManagement();
	}
	
	@Test
	public void testLoginEmptyStrings(){
		String user = "";
		String pw = "";
		try {
			smgmt.login(user,pw);
			fail();
		} catch (InvalidParameterException e) {
			// success
		}
	}
	
	@Test
	public void testLoginLogoutExistingUser(){
		Session session1 = null;
		Session session2 = null;
		
		try {
			session1 = smgmt.login("peter","secret");
			assertTrue(session1 instanceof Session);		// is Session object returned
			session2 = smgmt.login("pter", "secret");
			assertEquals(session1,session2);			// return same session when not logged out
			smgmt.login("klaus","supersecret");			
			fail();
		} catch (InvalidParameterException e) {			// user not existing
			//success
		}
		
		try {
			smgmt.login("peter","blub");				// wrong pw
			fail();
		} catch (InvalidParameterException e) {
			// success
		}
		
		try {
			smgmt.logout(session1);						// logout user
			session2 = smgmt.login("peter", "secret");	// new session
			assertNotSame(session1,session2);			// should be a new session
			smgmt.logout(null);							// invalid parameter
			fail();
		} catch (InvalidSessionException e) {
			// success									// null reference
		} catch (InvalidParameterException e) {
			fail();
		}
		
		try {
			smgmt.login(null,null);						// invalid prarameter
			fail();
		} catch (InvalidParameterException e) {
			// success
		}
		
	}
	
	@Test
	public void testCheckSessionActiveSession(){
		Session session1 = null;
		try {
			session1 = smgmt.login("peter", "secret");
			smgmt.checkSession(session1);				// session is valid
			smgmt.checkSession(null);					// 
			fail();
		} catch (InvalidSessionException e) {
			// success
		} catch (InvalidParameterException e) {
			fail();
		}
	}
	
	@Test
	public void testGetUserForValue(){
		Session session1 = null;
		User peter = null;
		User user = null;
		
		try {
			peter = UserDirectory.getReference().getUser("peter");
			session1 = smgmt.login(peter.getUsername(), peter.getPassword());
			user = HahnSessionManagement.getUserForValue(session1);
		} catch (InvalidParameterException e) {
			fail();
		}
		assertEquals(peter,user);
	}
	@Test
	public void testAutorizationNoAuthorization(){
		Session session1 = null;
		
		try {
			session1 = smgmt.login("peter","secret");
			smgmt.checkAuthorization(session1, "user");
			smgmt.checkAuthorization(session1, "eventmanager");	// user has both roles
		} catch ( NotAuthorizedException e) {
			fail();
		} catch (InvalidParameterException e) {
			fail();
		}
		
		try {
			session1 = smgmt.login("paul","secret");
			smgmt.checkAuthorization(session1, "eventmanager");	// has not this role
			fail();
		} catch (NotAuthorizedException e) {
			// success
		} catch (InvalidParameterException e) {
			fail();
		}
	}
	
	@Test
	public void testHasRole(){
		Session session1 = null;
		Session session2 = null;
		
		try {
			session1 = smgmt.login("peter", "secret");
			session2 = smgmt.login("paul", "secret");
		} catch (InvalidParameterException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		assertTrue(smgmt.hasRole(session1, "user"));
		assertTrue(smgmt.hasRole(session1, "eventmanager"));
		assertTrue(smgmt.hasRole(session2, "user"));
		assertFalse(smgmt.hasRole(session2, "eventmanager"));
	}
}
