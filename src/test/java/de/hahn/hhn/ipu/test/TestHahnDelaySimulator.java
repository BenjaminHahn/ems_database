package de.hahn.hhn.ipu.test;
import de.hahn.hhn.ipu.mock.HahnDelaySimulator;
import de.hhn.seb.ipu.app.exceptions.InvalidParameterException;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import org.junit.Before;
import org.junit.Test;

/**
 * @author Benjamin
 *
 */
public class TestHahnDelaySimulator {

	private HahnDelaySimulator sim;
	
	@Before
	public void initialize(){
		sim = new HahnDelaySimulator();
	}
	
	@Test
	public void testDefaultSimulator(){
		long minTime = sim.getMinimalDelayTime1();
		long maxTime = sim.getMaximalDelayTime1();
		assertEquals(minTime,1000);
		assertEquals(maxTime,1000);
	}
	
	@Test
	public void testSetDelayTimesNormalFunction(){
		int min = 0;
		int max = 99;
		try {
			sim.setDelayTimes(min, max);	// expected function
		} catch (InvalidParameterException e) {
			fail();
			e.printStackTrace();
		}
		assertEquals(min,sim.getMinimalDelayTime1());
		assertEquals(max, sim.getMaximalDelayTime1());
		assertFalse(sim.getMinimalDelayTime1() < 0);	
		assertTrue(sim.getMinimalDelayTime1() <= sim.getMaximalDelayTime1());
	}
	
	@Test
	public void testSetDelayTimesMinLessZero(){
		try {
			sim.setDelayTimes(-1,10);
			fail("Min DelayTime should not be negative");
		} catch (InvalidParameterException e) {
			// success
		}
	}
	
	@Test
	public void testSetDelayTimesMinLessMax(){
		try {
			sim.setDelayTimes(10, 5);
			fail("MinDelayTime should not be greater than MaxDelayTime");
		} catch (InvalidParameterException e) {
			// success
		}
	}
	
	@Test
	public void testSimulateDelay(){
		long currentTime = System.currentTimeMillis();
		long delayTime = 1000;	// DelayTime darf nicht zu klein sein
		
		try {
			sim.setDelayTimes(delayTime,delayTime);
			sim.simulateDelay();
		} catch (InvalidParameterException e) {
			fail();
		} catch (InterruptedException e) {
			fail();
		}
		long currentTime2 = System.currentTimeMillis();
		assertEquals(delayTime,currentTime2 - currentTime,delayTime * 0.1); // toleranz wegen kleiner delayTime
	}
	
	@Test
	public void testGetNextDelayTimeBetweenIntervallBorders(){
		try {
			sim.setDelayTimes(0, 1);
		} catch (InvalidParameterException e) {
			fail();
		}
		long time = sim.getNextDelayTime();
		assertTrue(time <= sim.getMaximalDelayTime1());
		assertTrue(time >= sim.getMinimalDelayTime1());
	}
	
	@Test
	public void testGetNextDelayTimeTestEnds(){
		try {
			sim.setDelayTimes(0,0);
		} catch (InvalidParameterException e) {
			fail();
		}
		assertEquals(0,sim.getNextDelayTime());
	}
}
