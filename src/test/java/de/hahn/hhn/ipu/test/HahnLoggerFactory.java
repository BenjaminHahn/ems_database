package de.hahn.hhn.ipu.test;

import de.hahn.hhn.ipu.logging.HahnLoggingHelper;
import de.hhn.seb.ipu.app.exceptions.InvalidParameterException;
import de.hhn.seb.ipulab.tests.junit.logging.LoggerFactory;
import de.hhn.seb.pp.logging.LoggingHelper;
import de.hhn.seb.pp.logging.LoggingHelperAlreadyInitializedException;

public class HahnLoggerFactory implements LoggerFactory {

    @Override
    public LoggingHelper getReference() {
        return HahnLoggingHelper.getReference();
    }

    @Override
    public LoggingHelper initialize(String appName)
            throws InvalidParameterException,
            LoggingHelperAlreadyInitializedException {
        return HahnLoggingHelper.getReference().initialize(appName);
    }

    @Override
    public void tearDown() {
        HahnLoggingHelper.getReference().tearDown();
    }

}
