package de.hahn.hhn.ipu.test;

import de.hahn.hhn.ipu.mock.HahnBookingManagement;
import de.hahn.hhn.ipu.mock.HahnSessionManagement;
import de.hhn.seb.ipu.app.eventmgmt.Booking;
import de.hhn.seb.ipu.app.eventmgmt.Event;
import de.hhn.seb.ipu.app.eventmgmt.Location;
import de.hhn.seb.ipu.app.eventmgmt.Session;
import de.hhn.seb.ipu.app.exceptions.InvalidParameterException;
import de.hhn.seb.ipu.app.exceptions.NoReservationPossibleException;
import de.hhn.seb.ipu.app.exceptions.NotAuthorizedException;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.Date;
import java.util.logging.Logger;

import static org.junit.Assert.*;

public class TestHahnBookingManagement {
	/** Standard class logger. **/
	private static Logger logger = Logger
			.getLogger(TestHahnBookingManagement.class.getName());
	
	private static HahnBookingManagement bmgmt;
	private static Event event = null;
	private static Booking booking1 = null;
	private static Booking booking2 = null;
	private static Booking booking3 = null;
	
	private HahnSessionManagement smgmt = new HahnSessionManagement();
	
	@BeforeClass
	public static void initializeClass(){
		
		event = new Event("eventtitle1", "keine beschreibung", new Location("name1","description1",null), new Date(), new Date());
		event.setReservationPossible(true);
        event.setMaxNoParticipants(10);
		try {
			bmgmt = new HahnBookingManagement(event,HahnSessionManagement.getReference());
		} catch (InvalidParameterException e) {
			fail();
		}
		booking1 = new Booking(event.getId(),1);	// usable mock data
		booking1.setUserName("peter");
		booking2 = new Booking(event.getId(),2);
		booking2.setUserName("paul");
		booking3 = new Booking(event.getId(),3);
		booking3.setUserName("mary");
	}
	@Before
	public void initialize(){
		event.setReservationPossible(true);		// before each Test
		bmgmt.removeBookings();	// remove all booking before every test
	}
	
//	@Ignore
//	public void testConstructorAndReference(){
//		try {
//			bmgmt = new HahnBookingManagement(event,HahnSessionManagement.getReference());	// null reference
//			fail();
//		} catch (InvalidParameterException e1) {
//			// success
//		}
//
//		assertEquals(bmgmt.getRepresentsEvent().getId(),event.getId()); // bookingmgmt should be set to belonging event
//
//		// get bookingmanagement for event
//		HahnBookingManagement bmgmt2 = HahnBookingManagement.getBookingManagementViaEventId(event.getId());
//		assertEquals(bmgmt2,bmgmt);		// bookings for same event should be the same
//	}
	
	@Test
	public void testAddBooking(){
				
		try {
			bmgmt.addBooking(booking1);
			event.setReservationPossible(false);
			bmgmt.addBooking(booking2);
			fail();
			} catch (NoReservationPossibleException e) {	// reservation impossible
			// success
		}
		assertTrue("should be 1 but is " + bmgmt.getBookingCollection().size(), bmgmt.getBookingCollection().size() == 1);
	}
	
	@Test
	public void testGetBookings(){
		Session session = null;
		try {
			session = smgmt.login("peter", "secret");
			bmgmt.addBooking(booking1);
			bmgmt.addBooking(booking2);
		} catch (InvalidParameterException | NoReservationPossibleException e) {
			fail();
		}
		assertTrue(bmgmt.getBookings(session).size() == 2);	// should get both bookings
		
			
		try {
			session = smgmt.login("paul", "secret");
		} catch (InvalidParameterException e) {
			fail();
		}
		assertTrue(bmgmt.getBookings(session).size() == 1);	// should only get own booking
		
	}
	
	@Test
	public void testRemoveBooking(){
		Session session = null;
		
		try {
			session = smgmt.login("paul", "secret");		// remove booking as owner
			bmgmt.addBooking(booking2);
			bmgmt.removeBooking(session, booking2.getId());
		} catch (InvalidParameterException e) {
			fail();
		} catch (NotAuthorizedException e) {
			fail();
		} catch (NoReservationPossibleException e) {
			fail();
		}
		assertTrue(bmgmt.getBookingCollection().size() == 0);	// boooking removed
		
		try {
			session = smgmt.login("paul", "secret");		// remove booking as owner
			bmgmt.addBooking(booking3);
			bmgmt.removeBooking(session, booking3.getId());
		} catch (InvalidParameterException e) {
			fail();
		} catch (NotAuthorizedException e) {				// user is not autorized
			// success
		} catch (NoReservationPossibleException e) {
			fail();
		}
		assertTrue(bmgmt.getBookingCollection().size() == 1);	// booking not removed
		
		try {
			session = smgmt.login("peter", "secret");		// remove booking as eventmanager of user
			bmgmt.removeBooking(session, booking3.getId());
		} catch (InvalidParameterException e) {
			fail();
		} catch (NotAuthorizedException e) {
			fail();
		}
		assertTrue(bmgmt.getBookingCollection().size() == 0);	// booking removed
	}
	
	@Test
	public void testRemoveBookingInvalidParameterException(){
		Session session = null;
		
		try {
			session = smgmt.login("mary", "secret");
			bmgmt.addBooking(booking1);
			bmgmt.removeBooking(session, "someBookingId");	//booking id does not match
			fail();
		} catch (InvalidParameterException e) {
			//success
		} catch (NoReservationPossibleException e) {
			fail();
		} catch (NotAuthorizedException e) {
			fail();
		}
	}

    @Test
    public void testGetBookingManagementViaEventId() {
        Event event1 = new Event("title1",null,null,null,null);
        Event event2 = new Event("title1",null,null,null,null);
        HahnBookingManagement bmgmt1 = null;
        HahnBookingManagement bmgmt2 = null;
        try {
            bmgmt1 = new HahnBookingManagement(event1, smgmt);
            bmgmt2 = new HahnBookingManagement(event2, smgmt);
        } catch (InvalidParameterException e) {
            e.printStackTrace();
        }
//        HahnBookingManagement bmgmt3 = HahnBookingManagement.getBookingManagementViaEventId(event1.getId());
//        HahnBookingManagement bmgmt4 = HahnBookingManagement.getBookingManagementViaEventId(event1.getId());
//        HahnBookingManagement bmgmt5 = HahnBookingManagement.getBookingManagementViaEventId(event1.getId());
        assertNotSame(bmgmt2,bmgmt1);
    }
}
