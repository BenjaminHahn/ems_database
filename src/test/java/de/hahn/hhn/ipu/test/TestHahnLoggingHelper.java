package de.hahn.hhn.ipu.test;

import de.hahn.hhn.ipu.logging.HahnLoggingHelper;
import de.hhn.seb.ipu.app.exceptions.InvalidParameterException;
import de.hhn.seb.pp.logging.FileHandlerAlreadyInstantiatedException;
import de.hhn.seb.pp.logging.LoggingHelperAlreadyInitializedException;
import de.hhn.seb.pp.logging.LoggingHelperNotInitializedException;
import de.hhn.seb.pp.logging.WhiteListBlackListConflictException;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import org.junit.Before;
import org.junit.Test;

public class TestHahnLoggingHelper {
	/** Standard class logger. **/
	private static Logger logger = Logger.getLogger(TestHahnLoggingHelper.class
			.getName());
    @Before
    public void ini() {
        //HahnLoggingHelper helper = HahnLoggingHelper.getReference();
        //helper.tearDown();
    }
    
	@Test
	public void testGetReferenceAndInitialize(){
	    HahnLoggingHelper helper = HahnLoggingHelper.getReference();
	    try {
            helper.initialize("test");
        } catch (LoggingHelperAlreadyInitializedException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        } catch (InvalidParameterException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }
		HahnLoggingHelper helper1 = HahnLoggingHelper.getReference();
		assertEquals(helper1,helper);
		
		try {
			helper.initialize("test");
			fail();
		} catch (LoggingHelperAlreadyInitializedException e) {
			// success already inizialized
		} catch (InvalidParameterException e) {
			fail();
		}
		
		Level level = null;
		try {
			level = helper.getLogLevel();
		} catch (LoggingHelperNotInitializedException e) {
			fail();
		}
		assertEquals(level,Level.FINEST);		// Level should be finest
		
		try {
			helper.setLogLevel(Level.WARNING);
			level = helper.getLogLevel();
		} catch (LoggingHelperNotInitializedException e) {
			fail();
		} catch (InvalidParameterException e) {
			fail();
		}
		assertEquals(level,Level.WARNING);		// new level set
	}
	
	
	@Test
	public void testBlackList(){
		HahnLoggingHelper helper = HahnLoggingHelper.getReference();
		Logger testLogger = Logger.getLogger("test.blub.bla");
		
		
		try {
            helper.addLoggerPrefixBlackList("test.blub.bla");
        } catch (LoggingHelperNotInitializedException
                | WhiteListBlackListConflictException
                | InvalidParameterException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
		
		testLogger.warning("TESTLOGGERWARNING");
		
	}
	
   @Test
    public void testWhiteList(){
        HahnLoggingHelper helper = HahnLoggingHelper.getReference();
        try {
            helper.initialize("test");
        } catch (LoggingHelperAlreadyInitializedException
                | InvalidParameterException e1) {
            fail();
        }
        Logger allowedLogger1 = Logger.getLogger("test.blub.bla");
        Logger allowedLogger2 = Logger.getLogger("test");
        Logger unallowedLogger = Logger.getLogger("tescht.blub");
        
        try {
            helper.addLoggerPrefixWhiteList("test");
            allowedLogger1.info("allowed 1");
            allowedLogger2.info("allowed 2");
            unallowedLogger.info("unallowed");
        } catch (LoggingHelperNotInitializedException
                | WhiteListBlackListConflictException
                | InvalidParameterException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
	
	@Test
	public void testPrintLoggs(){
        logger.severe("severe Warning!");
        logger.warning("warning Warning!");
	    logger.info("info Warning!");
	}
	
	@Test
	public void testFileHandler(){
        HahnLoggingHelper helper = HahnLoggingHelper.getReference();
        try {
            helper.createFileHandler();
        } catch (LoggingHelperNotInitializedException e) {
            fail();
        } catch (IOException e) {
            fail();
        } catch (FileHandlerAlreadyInstantiatedException e) {
            fail();
        }
	    logger.warning("filehandler");
	}
	
	@Test
	public void testTearDown() {
	    HahnLoggingHelper helper = HahnLoggingHelper.getReference();
	    try {
            helper.initialize("test");
        } catch (LoggingHelperAlreadyInitializedException
                | InvalidParameterException e) {
            fail();
        }
        helper.tearDown();
        HahnLoggingHelper helper1 = HahnLoggingHelper.getReference();
        try {
            helper1.initialize("otherTest");
        } catch (LoggingHelperAlreadyInitializedException e) {
            fail();
        } catch (InvalidParameterException e) {
            fail();
        }
	}
	
	@Test
	public void testLogLevel(){
        HahnLoggingHelper helper = HahnLoggingHelper.getReference();
        try {
            helper.initialize("test");
        } catch (LoggingHelperAlreadyInitializedException
                | InvalidParameterException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }
        logger.info("message info");
        logger.finest("message info should not logged");
        try {
            helper.setLogLevel(Level.FINEST);
        } catch (LoggingHelperNotInitializedException
                | InvalidParameterException e) {
            fail();
        }
        logger.finest("message finest");
        
	}
}
