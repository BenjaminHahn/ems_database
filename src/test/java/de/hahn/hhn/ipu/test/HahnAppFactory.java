/**
 * 
 */
package de.hahn.hhn.ipu.test;

import de.hahn.hhn.ipu.mock.HahnBookingManagement;
import de.hahn.hhn.ipu.mock.HahnDelaySimulator;
import de.hahn.hhn.ipu.mock.HahnEventManagement;
import de.hahn.hhn.ipu.mock.HahnSessionManagement;
import de.hhn.seb.ipu.app.eventmgmt.BookingManagement;
import de.hhn.seb.ipu.app.eventmgmt.Event;
import de.hhn.seb.ipu.app.eventmgmt.EventManagementService;
import de.hhn.seb.ipu.app.eventmgmt.SessionManagement;
import de.hhn.seb.ipu.app.exceptions.InvalidParameterException;
import de.hhn.seb.ipu.app.mock.DelaySimulator;
import de.hhn.seb.ipu.app.tests.AppFactory;
import java.util.logging.Logger;

/**
 * @author Ben
 *
 */
public class HahnAppFactory implements AppFactory {
	/** Standard class logger. **/
	private static Logger logger = Logger.getLogger(HahnAppFactory.class
			.getName());

	/* (non-Javadoc)
	 * @see de.hhn.seb.ipu.app.tests.AppFactory#createDelaySimulator(long, long)
	 */
	@Override
	public DelaySimulator createDelaySimulator(long minimalDelayTime,
			long maximalDelayTime) throws InvalidParameterException {
		return new HahnDelaySimulator(minimalDelayTime,
				maximalDelayTime);
	}

	/* (non-Javadoc)
	 * @see de.hhn.seb.ipu.app.tests.AppFactory#createEventManagementService()
	 */
	@Override
	public EventManagementService createEventManagementService() {
		return new HahnEventManagement();
	}

	/* (non-Javadoc)
	 * @see de.hhn.seb.ipu.app.tests.AppFactory#createSessionManagement()
	 */
	@Override
	public SessionManagement createSessionManagement() {
		return new HahnSessionManagement();
	}

	/* (non-Javadoc)
	 * @see de.hhn.seb.ipu.app.tests.AppFactory#createBookingManagement(de.hhn.seb.ipu.app.eventmgmt.Event, de.hhn.seb.ipu.app.eventmgmt.SessionManagement)
	 */
	@Override
	public BookingManagement createBookingManagement(Event event,
			SessionManagement sessionManagement) {
		try {
			return new HahnBookingManagement(event, new HahnSessionManagement());
		} catch (InvalidParameterException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
}
